$(document).ready(function () {
			
	$("#country_id").change(function() {
		$("#region_id").html("");
		$("#city_id").html("");
		$("#sort").submit();
	});
	
	$("#region_id").change(function() {
		var regionId = $(this).val();
		$("#city_id").html("");
		if (regionId > 0) {
			$("#sort").submit();
		}
	});
		
	$("#city_id").change(function() {
		var cityId = $(this).val();
		if (cityId > 0) {
			$("#sort").submit();
		}
	}); 
});	