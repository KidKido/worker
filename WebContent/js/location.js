$(document).ready(function() {
	
	$("#country_id").change(function() {
		var countryId = $("#country_id").val();
		var url = 'regions.action?country_id=' + countryId;
		$("#region_id").html("");
		$("#city_id").html("");
		
		if (countryId > 0) {
			
			$.getJSON(url, function (json) {
				// Массив объектов
				var regions = json.regions;
				
				var html = "";
				html = html + "<option value=\"\">Выберите регион</option>";
				for (var i = 0; i < regions.length; i++) {
					html = html + "<option value=\"" + regions[i].id + "\">" + regions[i].name + "</option>";
				}
				
				$("#region_id").html(html);
				$("#region_id").show();
			});
		}
	});
		
	
	$("#region_id").change(function() {
		var regionId = $("#region_id").val();
		if (regionId !== "") {
			var url = 'cities.action?region_id=' + regionId;
			
			$.getJSON(url, function(json) {
				var cities = json.cities;

				var html = "";
				html = html + "<option value=\"\">Выберите город</option>";
				for (var i = 0; i < cities.length; i++) {
					html = html + "<option value=\"" + cities[i].id + "\">" + cities[i].name + "</option>";
				}
				
				$("#city_id").html(html);
				
			});
		} else {
			$("#city_id").html("");
		}
	});
	
});