<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="http://getbootstrap.com/favicon.ico">
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="webjars/jquery/2.1.1/jquery.min.js"></script>
<script src="webjars/bootstrapvalidator/0.5.0/js/bootstrapValidator.min.js"></script>
<script src="js/sort.js"></script>
<script src="js/bootstrap.min.js"></script>
<link href="webjars/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
<link href="webjars/bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css" rel="stylesheet">
<link rel="stylesheet" href="css/style.css">
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed"
        data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span> <span
        class="icon-bar"></span> <span class="icon-bar"></span> <span
        class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="."><span style="color: blue;">Wor</span><span style = "color: yellow;">ker</span></a>
      <c:if test="${user != null}">
        <h4>С возвращением,
          <b>${user.getFirstName()} ${user.getLastName()}</b><span class="label label-default"></span>
        </h4>
      </c:if>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <c:if test="${user == null}">
          <li class="active"><a href="signin.action">Войти <span
            class="sr-only">(current)</span></a></li>
          <li><a href="signup.action">Регистрация</a></li>
        </c:if>
        <c:if test="${user != null}">
          <form class="navbar-form navbar-left" action="signin.action"
            method="post">
            <input type="hidden" name="signout">
            <button type="submit" class="btn btn-default">Выход</button>
          </form>
        </c:if>
      </ul>
      <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="search" name="search" class="form-control"
            placeholder="Введите запрос">
        </div>
        <button type="submit" class="btn btn-default">Поиск</button>
      </form>
      <c:if test="${user != null}">
        <ul class="nav navbar-nav navbar-right">
          <c:if test="${user.getRole() == 1}">
            <li><a href="new-tender.action">Создать тендер</a></li>
          </c:if>
          <li><a href="profile.action">Профиль</a></li>
          <c:if test="${user.getRole() == 1}">
                <li><a href="mytender.action">Мои тендеры</a></li>
              </c:if>
        </ul>
      </c:if>
    </div>
  </div>
</nav>
