<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Профиль</title>
    <script src="//code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="js/location.js"></script>
    <link rel="stylesheet" href="css/style.css">
    <script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>
    <script>
      webshims.setOptions('forms-ext', {types: 'date'});
      webshims.polyfill('forms forms-ext');
    </script>
  </head>
  <body>
    <%@ include file="header.jsp" %>
    <form method="post">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
            <c:if test="${saveChange}">
              <div class="alert alert-success" role="alert" style="text-align: center; font-weight: bold;">Данные сохранены</div>
            </c:if>
            <div class="panel panel-info">
              <div class="panel-heading">
                <h3 class="panel-title">Редактирование профиля</h3>
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=100" class="img-circle"> </div>
                  <div class=" col-md-9 col-lg-9 ">
                    <table class="table table-user-information">
                      <tbody>
                        <tr>
                          <td>Имя</td>
                          <td><input style="border: none;" type="text" name="first_name" value="${user.getFirstName()}"></td>
                        </tr>
                        <tr>
                          <td>Фамилия</td>
                          <td><input style="border: none;" type="text" name="surname" value="${user.getSurname()}"></td>
                        </tr>
                        <tr>
                          <td>Отчество</td>
                          <td><input style="border: none;" type="text" name="last_name" value="${user.getLastName()}"></td>
                        </tr>
                        <c:if test="${user.getRole() == 0}">
                          <tr>
                            <td>Профессия</td>
                            <td><input style="border: none;" type="text" name="profession" value="${user.getProfession()}"></td>
                          </tr>
                          <tr>
                            <td>Специальность</td>
                            <td><input style="border: none;" type="text" name="position" value="${user.getPosition()}"></td>
                          </tr>
                          <tr>
                            <td>Стаж</td>
                            <td><input style="border: none;" type="text" name="experience" value="${user.getExperience()}"></td>
                          </tr>
                        </c:if>
                        <tr>
                          <td>Дата рождения</td>
                          <td><input style="border: none;" type="date" name="birth_date" pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])" value="${birthDate}" style="border: medium none;"></td>
                        </tr>
                        <tr>
                          <td>Страна проживания</td>
                          <td>
                            <select id="country_id" style="border: none; width: 206px;" name="country_id" required>
                              <option value="">Выберите страну</option>
                              <c:forEach var="each" items="${countriesList}">
                                <option value="${each.getId()}"
                                <c:if test="${each.getId() == user.getCountry().getId()}"> selected</c:if>
                                >${each.getName()}</option>
                              </c:forEach>
                            </select >
                          </td>
                        </tr>
                        <tr>
                          <td>Регион проживаия</td>
                          <td>
                            <select id="region_id" style="border: none; width: 206px;" name="region_id" required>
                              <option value="">Выберите регион</option>
                              <c:forEach var="each" items="${regionsList}">
                                <option value="${each.getId()}"
                                <c:if test="${each.getId() == user.getRegion().getId()}"> selected</c:if>
                                >${each.getName()}</option>
                              </c:forEach>
                            </select >
                          </td>
                        </tr>
                        <tr>
                          <td>Город проживания</td>
                          <td>
                            <select id="city_id" style="border: none; width: 206px;" name="city_id" required>
                              <option value="">Выберите город</option>
                              <c:forEach var="each" items="${citiesList}">
                                <option value="${each.getId()}"
                                <c:if test="${each.getId() == user.getCity().getId()}"> selected</c:if>
                                >${each.getName()}</option>
                              </c:forEach>
                            </select >
                          </td>
                        </tr>
                        <tr>
                          <td>Пол</td>
                          <td>
                            <select style="border: none; padding-right: 123px;" name="gender" required>
                              <option value="1"
                              <c:if test="${genderText == 1}"> selected</c:if>
                              >Мужской</option>
                              <option value="0"
                              <c:if test="${genderText == 0}"> selected</c:if>
                              >Женский</option>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td>Email</td>
                          <td><input style="border: none;" type="email" name="email" value="${user.getContact().getEmail()}"></td>
                        </tr>
                        <tr>
                          <td>Контактный номер</td>
                          <td><input style="border: none;" type="text" name="phones" value="${user.getContact().getPhones()}"></td>
                        </tr>
                        <tr>
                          <td><img alt="User Pic" src="images/skype.png"> Skype</td>
                          <td><input style="border: none;" type="text" name="skype" value="${user.getContact().getSkype()}"></td>
                        </tr>
                        <tr>
                          <td><img alt="User Pic" src="images/facebook.png"> Facebook</td>
                          <td><input style="border: none;" type="text" name="facebook" value="${user.getContact().getFacebook()}"></td>
                        </tr>
                        <tr>
                          <td><img alt="User Pic" src="images/vkontakte.png"> Вконтакте</td>
                          <td><input style="border: none;" type="text" name="vkontakte" value="${user.getContact().getVkontakte()}"></td>
                        </tr>
                        <tr>
                          <td><img alt="User Pic" src="images/twitter.png"> Twitter</td>
                          <td><input style="border: none;" type="text" name="twitter" value="${user.getContact().getTwitter()}"></td>
                        </tr>
                        <tr>
                          <td><img alt="User Pic" src="images/icq.png"> ICQ</td>
                          <td><input style="border: none;" type="text" name="icq" value="${user.getContact().getIcq()}"></td>
                        </tr>
                        <tr>
                          <td><img alt="User Pic" src="images/googleplus.png"> Google+</td>
                          <td><input style="border: none;" type="text" name="google_plus" value="${user.getContact().getGooglePlus()}"></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="panel-footer">
                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Сохранить изменения"><br>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </body>
</html>
