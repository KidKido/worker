<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Worker</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">
    <link href="webjars/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="webjars/bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css" rel="stylesheet">
    <script src="webjars/jquery/2.1.1/jquery.min.js"></script>
    <script src="webjars/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="webjars/bootstrapvalidator/0.5.0/js/bootstrapValidator.min.js"></script>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/signin.css">
  </head>
  <body>
    <div class="container">
      <form class="form-signin" action="signin.action" method="post">
        <h2 class="form-signin-heading">Форма входа</h2>
        <input type="text" name="login" id="inputEmail" class="form-control" placeholder="Логин" required autofocus>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Пароль" required>
        <input class="btn btn-lg btn-primary btn-block" type="submit" value="Вход"><br>
        <c:if test="${notRegistered}">
          <div class="alert alert-danger" role="alert">Пользователь с такими данными не найден.<a href="signup.action">Зарегистрироваться?</a></div>
        </c:if>
      </form>
    </div>
  </body>
</html>