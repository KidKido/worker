<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">
    <link href="webjars/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="webjars/bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css" rel="stylesheet">
    <script src="webjars/jquery/2.1.1/jquery.min.js"></script>
    <script src="webjars/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="webjars/bootstrapvalidator/0.5.0/js/bootstrapValidator.min.js"></script>
    <link rel="stylesheet" href="css/style.css">
    <title>Регистрация</title>
    <script src="js/location.js"></script>
    <script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>
    <script>
      webshims.setOptions('forms-ext', {types: 'date'});
      webshims.polyfill('forms forms-ext');
    </script>
    <script type="text/javascript">
      $(document).ready(function() {
      
      	$("#customer").click(function() {
      		$("#worker_form").hide();
      	}); 
      					
      	$("#worker").click(function() {
      		$("#worker_form").show();
      	});
      	
      	$("#InputName").change(function() {
      		var login = $(this).val();
      		var url = 'check-login.action?login=' + login;
      		
      		$.getJSON(url, function(json) {
      			
      			var isRegisteredLogin = json.isAvailableLogin;
      			
      			if (isRegisteredLogin) {
      				$("#login-popup-username").html($("#InputName").val());
      				$("#login-popup").show();
      				$("#submit").prop("disabled", true);
      				$("#InputName").val("");
      			} else {
      				$("#login-popup").hide();
      				$("#submit").prop("disabled", false);
      			}
      		});
      	});	
      	
      	$("#email").change(function() {
      		var email = $(this).val();
      		var url = 'check-email.action?email=' + email;
      		
      		$.getJSON(url, function(json) {
      			
      			var isRegisteredEmail = json.isAvailableEmail;
      			
      			if (isRegisteredEmail) {
      				$("#email-popup-email").html($("#email").val());
      				$("#email-popup").show();
      				$("#submit").prop("disabled", true);
      				$("#email").val("");
      			} else {
      				$("#email-popup").hide();
      				$("#submit").prop("disabled", false);
      			}
      		});
      	});
      });
    </script>
  </head>
  <body>
    <%@ include file="header.jsp"%>
    <form method="post" id="registration" role="form">
      <div class="container">
        <div class="btn-group" data-toggle="buttons">
          <label id="customer" class="btn btn-primary active"><input id="customer" type="radio" value="1" name="work_form" checked required>Заказчик</label>
          <label id="worker" class="btn btn-primary"><input id="worker" type="radio" value="0" name="work_form"> Исполнитель</label>
        </div>
        <br>
        <br>
        <div class="row">
          <div class="col-md-6">
            <div class="well well-sm">
              <strong><span class="glyphicon glyphicon-asterisk"></span>Обязательное
              поле</strong>
            </div>
            <div class="form-group col-lg-6">
              <div class="form-group">
                <label for="InputName">Введите логин</label>
                <div class="input-group">
                  <input type="text" class="form-control" name="login"
                    id="InputName" placeholder="Ivanich" required> <span
                    class="input-group-addon"><span
                    class="glyphicon glyphicon-asterisk"></span></span>
                </div>
              </div>
            </div>
            <div class="form-group col-lg-6">
              <div class="form-group">
                <label for="Password">Пароль</label>
                <div class="input-group">
                  <input type="password" class="form-control" id="Password"
                    name="password" placeholder="Введите пароль" required> <span
                    class="input-group-addon"><span
                    class="glyphicon glyphicon-asterisk"></span></span>
                </div>
              </div>
            </div>
            <div class="form-group col-lg-6">
              <div class="form-group">
                <label for="Name">Имя</label>
                <div class="input-group">
                  <input type="text" class="form-control" id="Name"
                    name="first_name" placeholder="Иван" required> <span
                    class="input-group-addon"><span
                    class="glyphicon glyphicon-asterisk"></span></span>
                </div>
              </div>
            </div>
            <div class="form-group col-lg-6">
              <div class="form-group">
                <label for="Surname">Фамилия</label>
                <div class="input-group">
                  <input type="text" class="form-control" id="Name" name="surname"
                    placeholder="Иванов" required> <span
                    class="input-group-addon"><span
                    class="glyphicon glyphicon-asterisk"></span></span>
                </div>
              </div>
            </div>
            <div class="form-group col-lg-6">
              <div class="form-group">
                <label for="LastName">Отчество</label>
                <div class="input-group">
                  <input type="text" class="form-control" id="LastName"
                    name="last_name" placeholder="Иваныч"> <span
                    class="input-group-addon"></span>
                </div>
              </div>
            </div>
            <div class="form-group col-lg-6">
              <div class="form-group">
                <label for="BirthDate">Дата рождения</label>
                <div class="input-group">
                  <input type="date" class="form-control" id="BirthDate"
                    name="birth_date" placeholder="Два раза кликните"
                    required min="1900-01-01" max="1996-12-31" /> <span class="input-group-addon"><span
                    class="glyphicon glyphicon-asterisk"></span></span>
                </div>
              </div>
            </div>
            <div class="form-group col-lg-6">
              <div class="form-group">
                <label for="Phone">Телефон</label>
                <div class="input-group">
                  <input type="tel" class="form-control" id="Phone" name="phones"
                    placeholder="(050) 121-34-57" pattern="\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}" title="Формат ввода: (050) 121-34-57" required> <span class="input-group-addon"><span
                    class="glyphicon glyphicon-asterisk"></span></span>
                </div>
              </div>
            </div>
            <div class="form-group col-lg-6">
              <div class="form-group">
                <label for="email">Email</label>
                <div class="input-group">
                  <input type="email" class="form-control" id="email" name="email"
                    placeholder="test@gmail.com" required> <span
                    class="input-group-addon"><span
                    class="glyphicon glyphicon-asterisk"></span></span>
                </div>
              </div>
            </div>
            <div class="form-group col-lg-6">
              <div class="form-group">
                <label>Выберите страну: </label>
                <div class="input-group">
                  <select id="country_id" class="form-control" name="country_id"
                    required>
                    <option value="">Выберите страну</option>
                    <c:forEach var="each" items="${countriesList}">
                      <option value="${each.getId()}"
                      <c:if test="${each.getId() == user.getCountry().getId()}"> selected</c:if>
                      >${each.getName()}</option>
                    </c:forEach>
                  </select>
                  <span class="input-group-addon"><span
                    class="glyphicon glyphicon-asterisk"></span></span>
                </div>
              </div>
            </div>
            <div class="form-group col-lg-6">
              <div class="form-group">
                <label>Выберите регион: </label>
                <div class="input-group">
                  <select id="region_id" class="form-control" name="region_id"
                    required>
                    <option value="">Выберите регион</option>
                    <c:forEach var="each" items="${regionsList}">
                      <option value="${each.getId()}"
                      <c:if test="${each.getId() == user.getRegion().getId()}"> selected</c:if>
                      >${each.getName()}</option>
                    </c:forEach>
                  </select>
                  <span class="input-group-addon"><span
                    class="glyphicon glyphicon-asterisk"></span></span>
                </div>
              </div>
            </div>
            <div class="form-group col-lg-6">
              <div class="form-group">
                <label>Выберите город: </label>
                <div class="input-group">
                  <select id="city_id" class="form-control" name="city_id" required>
                    <option value="">Выберите город</option>
                    <c:forEach var="each" items="${citiesList}">
                      <option value="${each.getId()}"
                      <c:if test="${each.getId() == cityId}"> selected</c:if>
                      >${each.getName()}</option>
                    </c:forEach>
                  </select>
                  <span class="input-group-addon"><span
                    class="glyphicon glyphicon-asterisk"></span></span>
                </div>
              </div>
            </div>
            <div class="form-group col-lg-6">
              <div class="form-group">
                <label>Пол: </label>
                <div class="input-group">
                  <select class="form-control" name="gender">
                    <option value="1"
                    <c:if test="${genderText == 1}"> selected</c:if>
                    >Мужской</option>
                    <option value="0"
                    <c:if test="${genderText == 0}"> selected</c:if>
                    >Женский</option>
                  </select>
                  <span class="input-group-addon"><span
                    class="glyphicon glyphicon-asterisk"></span></span>
                </div>
              </div>
            </div>
            <div id="worker_form" style="display: none">
              <div class="form-group col-lg-6">
                <div class="form-group">
                  <label for="Profesion">Профессия: </label>
                  <div class="input-group">
                    <input type="text" class="form-control" id="profesion"
                      name="profession" placeholder="Электрик"> <span
                      class="input-group-addon"><span
                      class="glyphicon glyphicon-asterisk"></span></span>
                  </div>
                </div>
              </div>
              <div class="form-group col-lg-6">
                <div class="form-group">
                  <label for="Positio">Специальность: </label>
                  <div class="input-group">
                    <input type="text" class="form-control" id="Position"
                      name="position" placeholder="Розетки, проводка, электросеть">
                    <span class="input-group-addon"><span
                      class="glyphicon glyphicon-asterisk"></span></span>
                  </div>
                </div>
              </div>
              <div class="form-group col-lg-6">
                <div class="form-group">
                  <label for="Exp">Стаж: </label>
                  <div class="input-group">
                    <input type="text" class="form-control" id="Exp"
                      name="experience" placeholder="10 лет и 5 месяцев"> <span
                      class="input-group-addon"><span
                      class="glyphicon glyphicon-asterisk"></span></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <h3 class="dark-grey">Информация для регистрирующихся</h3>
            <p>"Заказчик" может создавать тендеры, однако он не сможет сам же учавствовать в них или в других тендерах.</p>
            <p>"Исполнитель" может учавствовать в тендерах, выдвигать свои предложения и озвучивать свое мнение об тендерах. "Исполнитель" не может создавать тендеры.</p>
            <p>Указывайте свои достоверные данные иначе, в случае предоставления ложных данных, ваш аккуант будет заблокирован</p>
            <p>Логин нужен для авторизации на сайте. Никому и никогда не давайте свой пароль от аккаунта!</p>
            <div style="display: none;" class="alert alert-danger" role="alert" id="login-popup">Логин&nbsp;<span style="font-weight: bold;" id="login-popup-username"></span> уже зарегистрирован</div>
            <div style="display: none;" id="email-popup" class="alert alert-danger" role="alert">E-mail&nbsp;<span style="font-weight: bold;" id="email-popup-email"></span> уже зарегистрирован</div>
            <p style="text-align: center;"><button type="submit" class="btn btn-primary">Зарегистрироваться</button>
            <p>
          </div>
        </div>
      </div>
    </form>
  </body>
</html>
