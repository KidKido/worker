<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Мои тендеры</title>
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <%@ include file="header.jsp" %>
    <c:forEach var="each" items="${userTendersList}">
      <hr style="margin-left: 30%; margin-right: 30%;">
      <fieldset style="margin-top: 30px; margin-left: 30%; margin-right: 30%;">
        <legend style="border-style: none;"><a href="tender.action?id=${each.getId()}">${each.getTitle()} - ${each.getCity().getName()}</a></legend>
        <div style="height: 150px;">${each.getShortDescription()}</div>
        <div>
          <c:if test="${each.getDaysToEnd() < 0 }">
            <div style="float:left;">Тендер проводился с ${each.getCreationDate()} по ${each.getExpiryDate()}</div>
          </c:if>
          <c:if test="${each.getDaysToEnd() >= 0 }">
            <div style="float: left;">&nbsp;${each.getDaysToEnd()} дней до конца</div>
          </c:if>
          <div class="budget">Бюджет: ${each.getBudget()} грн.</div>
        </div>
      </fieldset>
      <hr style="margin-left: 30%; margin-right: 30%;">
    </c:forEach>
  </body>
</html>
