<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Добавление нового тендера</title>
    <script src="//code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="js/location.js"></script>
    <link rel="stylesheet" href="css/style.css">
    <script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>
    <script>
      webshims.setOptions('forms-ext', {types: 'date'});
      webshims.polyfill('forms forms-ext');
    </script>
  </head>
  <body>
    <%@ include file="header.jsp"%>
    <form method="post" id="newtender">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <div class="well well-sm">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="name">
                    Заголовок нового тендера*</label>
                    <input type="text" class="form-control" id="name" name="title" placeholder="Введите заголовок" required>
                  </div>
                  <div class="form-group">
                    <label for="email">
                    Бюджет*</label>
                    <div class="input-group">
                      <span class="input-group-addon">₴</span>
                      <input type="text" class="form-control" id="email" name="budget" placeholder="Введите бюджет тендера" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="subject">
                    Страна*</label>
                    <select id="country_id" name="country_id" class="form-control" required>
                      <option value="">Выберите страну</option>
                      <c:forEach var="each" items="${countriesList}">
                        <option value="${each.getId()}"
                        <c:if test="${each.getId() == countryId}"> selected</c:if>
                        >${each.getName()}</option>
                      </c:forEach>
                    </select >
                  </div>
                  <div class="form-group">
                    <label for="subject">
                    Регион*</label>
                    <select id="region_id" name="region_id" class="form-control" required>
                      <option value="">Выберите регион</option>
                      <c:forEach var="each" items="${regionsList}">
                        <option value="${each.getId()}"
                        <c:if test="${each.getId() == regionId}"> selected</c:if>
                        >${each.getName()}</option>
                      </c:forEach>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="subject">
                    Город*</label>
                    <select id="city_id" name="city_id" class="form-control" required>
                      <option value="">Выберите город</option>
                      <c:forEach var="each" items="${citiesList}">
                        <option value="${each.getId()}"
                        <c:if test="${each.getId() == cityId}"> selected</c:if>
                        >${each.getName()}</option>
                      </c:forEach>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="name">
                    Срок тендера*</label>
                    <input type="date" class="form-control" id="name" name="expiry" placeholder="Выберите дату" min="${minDate}" required>
                  </div>
                  <p>* - Обязательное поле</p>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="name">
                    Краткое описание тендера*</label>
                    <textarea rows="10" cols="20" name="short_description" id="message" class="form-control" rows="9" cols="25" required></textarea>
                  </div>
                </div>
                <div class="col-md-6" style="float: right;">
                  <div class="form-group">
                    <label for="name">
                    Полное описание тендера*</label>
                    <textarea rows="20" cols="50" name="long_description" id="message" class="form-control" rows="9" cols="25" required></textarea>
                  </div>
                </div>
                <div class="col-md-12">
                  <button type="submit" class="btn btn-primary pull-right" id="btnContactUs">
                  Создать тендер</button>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
          </div>
        </div>
      </div>
    </form>
  </body>
</html>
