<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Профиль</title>
    <script src="//code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="js/location.js"></script>
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <%@ include file="header.jsp" %>
    <form method="post">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
            <div class="panel panel-info">
              <div class="panel-heading">
                <h3 class="panel-title">${user.getSurname()} ${user.getFirstName()} ${user.getLastName()}</h3>
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=100" class="img-circle"> </div>
                  <div class=" col-md-9 col-lg-9 ">
                    <table class="table table-user-information">
                      <tbody>
                        <c:if test="${user.getRole() == 0}">
                          <tr>
                            <td>Специальность</td>
                            <td>${user.getProfession()}, ${user.getPosition()}</td>
                          </tr>
                          <tr>
                            <td>Стаж</td>
                            <td>${user.getExperience()}</td>
                          </tr>
                        </c:if>
                        <tr>
                          <td>Дата регистрации</td>
                          <td>${registrationDate}</td>
                        </tr>
                        <tr>
                          <td>Дата рождения</td>
                          <td>${birthDate}</td>
                        </tr>
                        <tr>
                          <td>Место жительства</td>
                          <td>${user.getCountry().getName()}, ${user.getRegion().getName()}, ${user.getCity().getName()}</td>
                        </tr>
                        <tr>
                          <td>Пол</td>
                          <c:if test="${user.getGender() == 1}">
                            <td>Мужской</td>
                          </c:if>
                          <c:if test="${user.getGender() == 0}">
                            <td>Женский</td>
                          </c:if>
                        </tr>
                        <tr>
                          <td>Email</td>
                          <td>${user.getContact().getEmail()}</td>
                        </tr>
                        <tr>
                          <td>Контактный номер</td>
                          <td>${user.getContact().getPhones()}</td>
                        </tr>
                        <tr>
                          <td><img alt="User Pic" src="images/skype.png"> Skype</td>
                          <td>${user.getContact().getSkype()}</td>
                        </tr>
                        <tr>
                          <td><img alt="User Pic" src="images/facebook.png"> Facebook</td>
                          <td>${user.getContact().getFacebook()}</td>
                        </tr>
                        <tr>
                          <td><img alt="User Pic" src="images/vkontakte.png"> Вконтакте</td>
                          <td>${user.getContact().getVkontakte()}</td>
                        </tr>
                        <tr>
                          <td><img alt="User Pic" src="images/twitter.png"> Twitter</td>
                          <td>${user.getContact().getTwitter()}</td>
                        </tr>
                        <tr>
                          <td><img alt="User Pic" src="images/icq.png"> ICQ</td>
                          <td>${user.getContact().getIcq()}</td>
                        </tr>
                        <tr>
                          <td><img alt="User Pic" src="images/googleplus.png"> Google+</td>
                          <td>${user.getContact().getGooglePlus()}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="panel-footer">
                <a href="edit.action" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning" title="Редактировать профиль">Редактировать профиль</a>
                <span class="pull-right">
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
    <form method="post" id="registration" style="border: 2px solid black; border-radius:5px;margin-top: 50px;margin-right: 150px;margin-left: 150px;">
    </form>
  </body>
</html>