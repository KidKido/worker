<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Worker</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">
    <link href="webjars/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="webjars/bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css" rel="stylesheet">
    <script src="webjars/jquery/2.1.1/jquery.min.js"></script>
    <script src="webjars/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="webjars/bootstrapvalidator/0.5.0/js/bootstrapValidator.min.js"></script>
    <script src="js/sort.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.js"></script>
    <link rel="stylesheet" href="css/style.css">
    <script>
      $(document).ready(function() {
      var fadeBackground = $(document.createElement("div")).hide().attr("id", "fade-background"),
      modalWindow = $(document.createElement("div")).hide().attr("id", "modal-window");
      $(document.body).append(fadeBackground, modalWindow);
      $(document).on("click", ".modal-link", function(e) {
      	$("#fade-background").css({ "opacity": "0.3" }).fadeIn("fast");
                     var newContents = $("#" + $(this).data("modal-target")).html();
                     $("#modal-window").html(newContents);
      	$("#modal-window").fadeIn("fast");
      	e.preventDefault();
      });
      $(document).on("click", "#fade-background", function() {
      	$("#fade-background").fadeOut("fast");
      	$("#modal-window").fadeOut("fast");
      });
      $(document).keypress(function(e) {
      	if(e.keyCode == 27) {
      		$("#fade-background").fadeOut("fast");
      		$("#modal-window").fadeOut("fast");
      	}
      });
      });
    </script>
  </head>
  <body>
    <%@ include file="header.jsp"%>
    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-md-9">
          <div class="thumbnail">
            <img class="img-responsive" src="http://placehold.it/800x300" alt="">
            <div class="caption-full">
              <h4 class="pull-right">Бюджет: ${tender.getBudget()} гривен</h4>
              <h4>${tender.getTitle()}</h4>
              <p>${tender.getLongDescription()}</p>
            </div>
            <div class="caption-full">
              <p><strong>Контакты:</strong></p>
              <p>${tender.getOwner().getFirstName()} ${tender.getOwner().getSurname()}</p>
              <p>Телефон: ${tender.getOwner().getContact().getPhones()}</p>
              <p>Email: ${tender.getOwner().getContact().getEmail()}</p>
            </div>
              <c:if test="${daysToEnd < 0 }">
                <p style="float: right; margin-right: 10px;">Тендер проводился с ${creationDay} по ${expiryDay}</p>
              </c:if>
              <c:if test="${daysToEnd > 3}">
                <p style="float: right; margin-right: 10px;">&nbsp;${daysToEnd} дней до конца</p>
              </c:if>
              <div class="ratings">
              	<c:if test="${daysToEnd >= 0 && daysToEnd <= 3}">
                	<p style="float: right;">&nbsp;${daysToEnd} дней до конца</p>
              	</c:if>
              </div>
              <p style="margin-left: 10px;">Местоположение: ${tender.getCountry().getName()}&nbsp; ${tender.getRegion().getName()}&nbsp; ${tender.getCity().getName()}</p>
          </div>
          <div class="well">
            <c:if test="${user != null}">
            </c:if>
            <c:if test="${user.getRole() == 0}">
              <c:if test="${daysToEnd >= 0 }">
                <div class="text-right">
                  <a href="javascript:void(0)" class="btn btn-success modal-link" id="clicker" data-modal-target="example-popup">Оставить предложение</a>
                </div>
              </c:if>
            </c:if>
            <div id="example-popup" class="modal-content">
              <form action="add-tender-message.action" method="post">
                <div class="pagination-centered">
                  <br>
                  <input type="hidden" value="${tender.getId()}" name="tenderId">
                  <br>
                  <div>Введите предлагаеммую сумму* </div>
                  <div class="input-group" style="margin-right: 35%; margin-left: 35%;">
                    <span class="input-group-addon">₴</span>
                    <input type="text" class="form-control" aria-label="Сумма" required name="price">
                    <span class="input-group-addon">.00</span>
                  </div>
                  <br>
                  <div>Введите ваше сообщение</div>
                  <textarea required class="form-control" rows="3" name="message" placeholder="Ваше сообщение" style="margin-left: auto; margin-right: auto; display: block; text-align: center; width: 756px; height: 307px;"></textarea>
                  <br>
                  <button type="submit" class="btn btn-default">Отправить заявку</button>
                  <br><br>
                </div>
              </form>
            </div>
            <hr>
            <c:if test="${daysToEnd >= 0 }">
              <c:if test="${messagesList.isEmpty()}">
                <div class="alert alert-info" role="alert">
                  <h5>На данный момент нет предложений</h5>
                </div>
              </c:if>
            </c:if>
            <c:if test="${daysToEnd < 0 }">
              <div class="alert alert-danger" role="alert">
                <h5>Тендер окончен</h5>
              </div>
            </c:if>
            <c:if test="${!messagesList.isEmpty()}">
              <c:forEach var="each" items="${messagesList}">
                <div class="row">
                  <div class="col-md-12">
                    <p><strong>${each.getOwner().getSurname()} ${each.getOwner().getFirstName()} ${each.getOwner().getLastName()}</strong></p>
                    <p>${each.getMessage()}</p>
                    <p><strong>Контакты:</strong></p>
                    <p>Телефон: ${each.getOwner().getContact().getPhones()}</p>
                    <p>Email: ${each.getOwner().getContact().getEmail()}</p>
                    <span class="pull-right"><strong>Предлагаемая сумма: ${each.getPrice()} гривен</strong></span>
                    <p></p>
                  </div>
                </div>
                <hr>
              </c:forEach>
            </c:if>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>