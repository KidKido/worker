<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Worker</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <%@ include file="header.jsp"%>
    <div id="fixblock">
      <form id="sort" style="float: left; margin-bottom: 30%;">
        <div class="form-group">
          <label for="subject">
          Страна</label>
          <select id="country_id" name="country_id" class="form-control" required>
            <option value="">Выберите страну</option>
            <c:forEach var="each" items="${countriesList}">
              <option value="${each.getId()}"
              <c:if test="${each.getId() == countryId}"> selected</c:if>
              >${each.getName()}</option>
            </c:forEach>
          </select >
        </div>
        <div class="form-group">
          <label for="subject">
          Регион</label>
          <select id="region_id" name="region_id" class="form-control" required>
            <option value="">Выберите регион</option>
            <c:forEach var="each" items="${regionsList}">
              <option value="${each.getId()}"
              <c:if test="${each.getId() == regionId}"> selected</c:if>
              >${each.getName()}</option>
            </c:forEach>
          </select>
        </div>
        <div class="form-group">
          <label for="subject">
          Город</label>
          <select id="city_id" name="city_id" class="form-control" required>
            <option value="">Выберите город</option>
            <c:forEach var="each" items="${citiesList}">
              <option value="${each.getId()}"
              <c:if test="${each.getId() == cityId}"> selected</c:if>
              >${each.getName()}</option>
            </c:forEach>
          </select>
        </div>
      </form>
    </div>
    <nav>
      <div class="pagination-centered">
        <ul class="pagination">
          <c:forEach var="each" items="${pageWrapper.getPages()}">
            <c:if test="${each == page}">
              <li class="active"><a
                href="?page=${each}&search=${search}&country_id=${countryId}&region_id=${regionId}&city_id=${cityId}">${each}</a></li>
            </c:if>
            <c:if test="${each != page}">
              <li><a
                href="?page=${each}&search=${search}&country_id=${countryId}&region_id=${regionId}&city_id=${cityId}">${each}</a></li>
            </c:if>
            &nbsp;
          </c:forEach>
        </ul>
      </div>
    </nav>
    <c:if test="${tendersList.isEmpty()}">
      <div class="container">
        <div class="alert alert-warning" role="alert" style="text-align: center;">Поиск не вернул результат</div>
      </div>
    </c:if>
    <!-- Page Content -->
    <div class="container">
      <c:forEach var="each" varStatus="varStatus" items="${tendersList}">
        <div class="row">
          <div class="col-md-7">
            <a href="#"> <img class="img-responsive"
              src="http://placehold.it/700x300" alt="">
            </a>
          </div>
          <div class="col-md-5">
            <h3><a href="tender.action?id=${each.getId()}">${each.getTitle()}</a></h3>
            <h4>${each.getCountry().getName()},
              ${each.getCity().getName()}
            </h4>
            <p>${each.getShortDescription()}</p>
            <br> <br>
            <p style="float: left; text-align: left;">${each.getDaysToEnd()}	дней до конца</p>
            <p class="budget">Бюджет: ${each.getBudget()} грн.</p>
          </div>
        </div>
        <c:if test="${varStatus.index != tendersList.size() - 1}">
        	<hr>
        </c:if>
      </c:forEach>
    </div>
    <nav>
      <div class="pagination-centered">
        <ul class="pagination">
          <c:forEach var="each" items="${pageWrapper.getPages()}">
            <c:if test="${each == page}">
              <li class="active"><a
                href="?page=${each}&search=${search}&country_id=${countryId}&region_id=${regionId}&city_id=${cityId}">${each}</a></li>
            </c:if>
            <c:if test="${each != page}">
              <li><a
                href="?page=${each}&search=${search}&country_id=${countryId}&region_id=${regionId}&city_id=${cityId}">${each}</a></li>
            </c:if>
            &nbsp;
          </c:forEach>
        </ul>
      </div>
    </nav>
  </body>
</html>
