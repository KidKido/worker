package com.worker.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.worker.entities.City;
import com.worker.entities.Contact;
import com.worker.entities.Country;
import com.worker.entities.Region;
import com.worker.entities.User;

@Repository
@SuppressWarnings("unchecked")
public class UserDao {

	@Autowired
	private SessionFactory sessionFactory;

	public List<User> getUserList() {
		List<User> usersList = sessionFactory.getCurrentSession()
				.createCriteria(User.class).list();
		return usersList;
	}

	public List<Country> getCountriesList() {
		List<Country> countriesList = sessionFactory.getCurrentSession()
				.createCriteria(Country.class).list();
		return countriesList;
	}

	public List<Region> getRegionsList(int countryId) {
		List<Region> regionsList = sessionFactory.getCurrentSession()
				.createCriteria(Region.class)
				.add(Restrictions.eq("countryId", countryId))
				.addOrder(Order.asc("name")).list();

		return regionsList;
	}

	public List<City> getCitiesList(int regionId) {
		List<City> citiesList = sessionFactory.getCurrentSession()
				.createCriteria(City.class)
				.add(Restrictions.eq("regionId", regionId))
				.addOrder(Order.asc("name")).list();

		return citiesList;
	}

	public void saveUser(User user) {
		Contact contact = saveContact(user.getContact());
		Session session = sessionFactory.getCurrentSession();

		user.setContact(contact);
		session.save(user);
		session.flush();
	}

	public Contact saveContact(Contact contact) {
		Session session = sessionFactory.getCurrentSession();
		session.save(contact);
		session.flush();

		contact = (Contact) session.createCriteria(Contact.class)
				.add(Restrictions.eq("email", contact.getEmail())).list()
				.get(0);

		return contact;
	}

	public boolean isExists(String login, String password) {
		Object uniqueResult = sessionFactory.getCurrentSession()
				.createCriteria(User.class)
				.add(Restrictions.eq("login", login))
				.add(Restrictions.eq("password", password)).uniqueResult();

		if (uniqueResult != null) {
			return true;
		}
		return false;
	}

	public void saveUUID(String login, String uuid) {
		Session session = sessionFactory.getCurrentSession();

		User user = (User) session.createCriteria(User.class)
				.add(Restrictions.eq("login", login)).uniqueResult();

		user.setUuid(uuid);

		session.update(user);
		session.flush();
	}

	public User getUser(String uuid) {
		Object result = sessionFactory.getCurrentSession()
				.createCriteria(User.class).add(Restrictions.eq("uuid", uuid))
				.uniqueResult();
		if (result == null) {
			return null;
		}

		User user = (User) result;
		return user;
	}

	public Region getRegion(int cityId) {
		City city = (City) sessionFactory.getCurrentSession()
				.createCriteria(City.class).add(Restrictions.eq("id", cityId))
				.uniqueResult();
		Region region = city.getRegion();

		return region;
	}

	public City getCity(int cityId) {
		City city = (City) sessionFactory.getCurrentSession()
				.createCriteria(City.class).add(Restrictions.eq("id", cityId))
				.uniqueResult();

		return city;
	}

	public Country getCountry(int countryId) {
		Country country = (Country) sessionFactory.getCurrentSession()
				.createCriteria(Country.class)
				.add(Restrictions.eq("id", countryId)).uniqueResult();

		return country;
	}

	public Contact getContact(int id) {
		Contact contact = (Contact) sessionFactory.getCurrentSession()
				.createCriteria(Contact.class).add(Restrictions.eq("id", id));

		return contact;
	}

	public void signOut(String uuid) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session
				.createQuery("update User set uuid = '' where uuid = :uuid");
		query.setParameter("uuid", uuid);
		query.executeUpdate();
	}

	public void updateUser(User user) {
		Session session = sessionFactory.getCurrentSession();
		session.update(user);
		session.flush();
	}

	public void updateContact(Contact contact) {
		Session session = sessionFactory.getCurrentSession();
		session.update(contact);
		session.flush();
	}

	public boolean isAvailableLogin(String login) {
		Object isAvailableLogin = sessionFactory.getCurrentSession()
				.createCriteria(User.class)
				.add(Restrictions.eq("login", login)).uniqueResult();

		if (isAvailableLogin != null) {
			return true;
	}
	return false;
}

	public boolean isAvailableEmail(String email) {
		Object isAvailableEmail = sessionFactory.getCurrentSession()
				.createCriteria(Contact.class)
				.add(Restrictions.eq("email", email)).uniqueResult();

		if (isAvailableEmail != null) {
			return true;
		}
		return false;
	}
}