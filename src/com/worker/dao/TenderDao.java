package com.worker.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.worker.entities.Tender;
import com.worker.entities.TenderMessage;

@Repository
public class TenderDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private UserDao userDao;

	@SuppressWarnings("unchecked")
	public List<Tender> getTendersList(String search, Integer countryId,
			Integer regionId, Integer cityId, int page) {
		String sql = "SELECT *, DATEDIFF(expiry_date, NOW()) AS days_to_end FROM tenders WHERE DATEDIFF(expiry_date, NOW()) >= 0";

		if (search != null) {
			if (!search.isEmpty()) {
				sql = sql + " AND MATCH title AGAINST ('+" + search
						+ "*' IN BOOLEAN MODE)";
			}
		}

		if (countryId != null) {
			sql = sql + " AND tenders.country_id = " + countryId;
		}

		if (regionId != null) {
			sql = sql + " AND tenders.region_id = " + regionId;
		}

		if (cityId != null) {
			sql = sql + " AND tenders.city_id = " + cityId;
		}

		sql = sql + " ORDER BY creation_date DESC";
		sql = sql + " LIMIT " + (page - 1) * 10 + ", " + 10;
		
		List<Tender> tendersList = sessionFactory.getCurrentSession()
				.createSQLQuery(sql).addEntity(Tender.class).list();
		
		for (Tender each : tendersList) {
			long currentTime = new Date().getTime();
			long diff = each.getExpiryDate().getTime() - currentTime;
			int daysToEnd = (int) (diff / (24 * 60 * 60 * 1000));
			each.setDaysToEnd(daysToEnd);
		}

		return tendersList;
	}

	public Tender getTender(int tenderId) {
		Tender tender = (Tender) sessionFactory.getCurrentSession()
				.createCriteria(Tender.class)
				.add(Restrictions.eq("id", tenderId)).uniqueResult();

		long currentTime = new Date().getTime();
		long diff = tender.getExpiryDate().getTime() - currentTime;
		int diffDays = (int) (diff / (24 * 60 * 60 * 1000));
		tender.setDaysToEnd(diffDays);

		return tender;
	}

	@SuppressWarnings("unchecked")
	public List<TenderMessage> getMessagesList(int tenderId) {
		List<TenderMessage> messagesList = sessionFactory.getCurrentSession()
				.createCriteria(TenderMessage.class)
				.add(Restrictions.eq("tenderId", tenderId)).list();
		return messagesList;
	}

	public void addTenderMessage(TenderMessage tenderMessage, int tenderId) {
		Session session = sessionFactory.getCurrentSession();
		Tender tender = getTender(tenderId);

		tenderMessage.setTender(tender);
		session.save(tenderMessage);
		session.flush();
	}

	public void addNewTender(Tender tender) {
		Session session = sessionFactory.getCurrentSession();
		session.save(tender);
		session.flush();
	}

	@SuppressWarnings("unchecked")
	public List<Tender> getUserTendersList(int userId) {
		List<Tender> userTendersList = sessionFactory.getCurrentSession()
				.createCriteria(Tender.class)
				.add(Restrictions.eq("ownerId", userId)).list();

		for (Tender each : userTendersList) {
			long currentTime = new Date().getTime();
			long diff = each.getExpiryDate().getTime() - currentTime;
			int daysToEnd = (int) (diff / (24 * 60 * 60 * 1000));
			each.setDaysToEnd(daysToEnd);
		}

		return userTendersList;
	}

	public int getPagesCount(String search, Integer countryId,
			Integer regionId, Integer cityId) {
		String sql = "SELECT COUNT(*) AS pages_count FROM tenders WHERE DATEDIFF(expiry_date, NOW()) >= 0 ";
		int pagesCount = 0;

		if (countryId != null) {
			sql = sql + " AND tenders.country_id = " + countryId;
		}

		if (regionId != null) {
			sql = sql + " AND tenders.region_id = " + regionId;
		}

		if (cityId != null) {
			sql = sql + " AND tenders.city_id = " + cityId;
		}

		if (search != null && !search.isEmpty()) {
			sql = sql + " AND MATCH title AGAINST ('+" + search
					+ "*' IN BOOLEAN MODE)";
		}

		Number number = (Number) sessionFactory.getCurrentSession()
				.createSQLQuery(sql).uniqueResult();
		pagesCount = number.intValue();

		return pagesCount;
	}

}