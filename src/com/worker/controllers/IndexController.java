package com.worker.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.worker.entities.City;
import com.worker.entities.Country;
import com.worker.entities.PageWrapper;
import com.worker.entities.Region;
import com.worker.entities.Tender;
import com.worker.entities.User;
import com.worker.services.TenderService;
import com.worker.services.UserService;

@Controller
public class IndexController {

	@Autowired
	private TenderService tenderService;
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/index.action", method = RequestMethod.GET)
	public ModelAndView getIndex(
			@RequestParam(value = "search", required = false) String search,
			@RequestParam(value = "country_id", required = false) Integer countryId,
			@RequestParam(value = "region_id", required = false) Integer regionId,
			@RequestParam(value = "city_id", required = false) Integer cityId,
			@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
			@CookieValue(value = "uuid", required = false) String uuid) {
		ModelAndView mav = new ModelAndView("index");

		mav.addObject("search", search);

		List<Country> countriesList = userService.getCountriesList();
		mav.addObject("countriesList", countriesList);

		if (countryId != null) {
			List<Region> regionsList = userService.getRegionsList(countryId);
			mav.addObject("regionsList", regionsList);
			mav.addObject("countryId", countryId);
		}

		if (regionId != null) {
			List<City> citiesList = userService.getCitiesList(regionId);
			mav.addObject("citiesList", citiesList);
			mav.addObject("regionId", regionId);
			mav.addObject("cityId", cityId);
		}

		mav.addObject("page", page);

		if (search != null || countryId != null || regionId != null
				|| cityId != null || page != null) {
			List<Tender> tendersList = tenderService.getTendersList(search,
					countryId, regionId, cityId, page);
			mav.addObject("tendersList", tendersList);
		}

		int pageCount = tenderService.getPagesCount(search, countryId, regionId, cityId);

		PageWrapper pageWrapper = new PageWrapper(pageCount, page);
		mav.addObject("pageWrapper", pageWrapper);

		User user = userService.getUser(uuid);
		mav.addObject("user", user);
		
		return mav;
	}

}