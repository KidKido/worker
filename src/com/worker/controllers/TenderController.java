package com.worker.controllers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.worker.entities.Tender;
import com.worker.entities.TenderMessage;
import com.worker.entities.User;
import com.worker.services.TenderService;
import com.worker.services.UserService;

@Controller
public class TenderController {

	@Autowired
	private TenderService tenderService;
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/tender.action", method = RequestMethod.GET)
	public ModelAndView getTender(
			@RequestParam(value = "id", required = true) Integer tenderId,
			@CookieValue(value = "uuid", required = false) String uuid) {
		ModelAndView mav = new ModelAndView("tender");

		Tender tender = tenderService.getTender(tenderId);
		mav.addObject("tender", tender);

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String expiryDay = dateFormat.format(tender.getExpiryDate());
		mav.addObject("expiryDay", expiryDay);
		
		String creationDay = dateFormat.format(tender.getCreationDate());
		mav.addObject("creationDay", creationDay);
		
		int daysToEnd = tender.getDaysToEnd();
		mav.addObject("daysToEnd", daysToEnd);

		List<TenderMessage> messagesList = tenderService
				.getMessagesList(tenderId);
		mav.addObject("messagesList", messagesList);

		if (uuid != null) {
			User user = userService.getUser(uuid);
			mav.addObject("user", user);
		}
		return mav;
	}

	@RequestMapping(value = "/add-tender-message.action", method = RequestMethod.POST)
	public ModelAndView postAddTenderMessage(
			@CookieValue(value = "uuid", required = true) String uuid,
			@RequestParam(value = "price", required = true) int price,
			@RequestParam(value = "message", required = true) String message,
			@RequestParam(value = "tenderId", required = true) int tenderId) {
		ModelAndView mav = new ModelAndView();

		if (uuid != null) {
			User user = userService.getUser(uuid);
			mav.addObject("user", user);

			TenderMessage tenderMessage = new TenderMessage();
			tenderMessage.setOwner(user);

			tenderMessage.setPrice(price);

			tenderMessage.setMessage(message);

			tenderMessage.setId(tenderId);

			tenderMessage.setCreationDate(new Date());

			tenderService.addTenderMessage(tenderMessage, tenderId);

			return new ModelAndView("redirect:tender.action?id= " + tenderId);
		}

		return mav;
	}

}
