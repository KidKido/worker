package com.worker.controllers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.worker.entities.City;
import com.worker.entities.Country;
import com.worker.entities.Region;
import com.worker.entities.Tender;
import com.worker.entities.User;
import com.worker.services.TenderService;
import com.worker.services.UserService;

@Controller
public class NewTenderController {
	
	@Autowired
	private TenderService tenderService;
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/new-tender.action", method = RequestMethod.GET)
	public ModelAndView getNewTender(@CookieValue(value = "uuid", required = true) String uuid) {
		ModelAndView mav = new ModelAndView("newtender");

		List<Country> countriesList = userService.getCountriesList();
		mav.addObject("countriesList", countriesList);
		
		User user = userService.getUser(uuid);
		mav.addObject("user", user);
		
		Date minamalDate = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String minDate = dateFormat.format(minamalDate); 
		// Передает переменную на jsp страницу
		mav.addObject("minDate", minDate);

		return mav;
	}

	@RequestMapping(value = "/new-tender.action", method = RequestMethod.POST)
	public ModelAndView postNewTender(
			@CookieValue(value = "uuid", required = true) String uuid,
			@RequestParam(value = "title", required = true) String title,
			@RequestParam(value = "short_description", required = false) String shortDescription,
			@RequestParam(value = "long_description", required = true) String longDescription,
			@RequestParam(value = "budget", required = true) String budgetText,
			@RequestParam(value = "expiry", required = true) String expiryDateText,
			@RequestParam(value = "country_id", required = false) Integer countryId,
			@RequestParam(value = "region_id", required = false) Integer regionId,
			@RequestParam(value = "city_id", required = false) Integer cityId) {
		ModelAndView mav = new ModelAndView("newtender");

		Tender tender = new Tender();

		if (uuid != null) {
			User user = userService.getUser(uuid);
			mav.addObject("user", user);
			tender.setOwner(user);
		}

		tender.setTitle(title);

		tender.setShortDescription(shortDescription);

		tender.setLongDescription(longDescription);

		if (!budgetText.isEmpty()) {
			int budget = Integer.parseInt(budgetText);
			tender.setBudget(budget);
		}

		tender.setCreationDate(new Date());

		Date expiryDate = null;
		if (!expiryDateText.isEmpty()) {
			try {
				expiryDate = new SimpleDateFormat("yyyy-MM-dd")
						.parse(expiryDateText);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			tender.setExpiryDate(expiryDate);
		}

		mav.addObject("countryId", countryId);
		Country country = new Country();
		country.setId(countryId);
		tender.setCountry(country);

		if (regionId != null) {
			mav.addObject("regionId", regionId);
			Region region = new Region();
			region.setId(regionId);
			tender.setRegion(region);
		}

		mav.addObject("tender", tender);

		if (cityId == null && regionId == null) {
			List<Country> countriesList = userService.getCountriesList();
			mav.addObject("countriesList", countriesList);

			List<Region> regionsList = userService.getRegionsList(countryId);
			mav.addObject("regionsList", regionsList);
		}

		if (cityId == null) {
			List<Country> countriesList = userService.getCountriesList();
			mav.addObject("countriesList", countriesList);

			List<City> citiesList = userService.getCitiesList(regionId);
			mav.addObject("citiesList", citiesList);

			List<Region> regionsList = userService.getRegionsList(countryId);
			mav.addObject("regionsList", regionsList);
		}

		mav.addObject("cityId", cityId);
		City city = new City();
		city.setId(cityId);
		tender.setCity(city);

		tenderService.addNewTender(tender);

		return new ModelAndView("redirect:index.action");
	}

}
