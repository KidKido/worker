package com.worker.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.worker.entities.Region;
import com.worker.services.UserService;

@Controller
public class RegionsController {

	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/regions.action", method = RequestMethod.GET)
	@ResponseBody
	public String getRegions(@RequestParam(value = "country_id", required = false) Integer countryId) {
		List<Region> regionsList = userService.getRegionsList(countryId);
		
		String json = "{\"regions\": [";
		for (Region each : regionsList) {
			json = json + "{\"id\":" + each.getId() + ",\"name\":\"" + each.getName() + "\"},";
		}
		json = json.substring(0, json.length() - 1);
		json = json + "]}";
		
		return json;
	}

}