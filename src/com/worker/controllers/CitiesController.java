package com.worker.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.worker.entities.City;
import com.worker.services.UserService;

@Controller
public class CitiesController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/cities.action", method = RequestMethod.GET)
	@ResponseBody
	public String getCities(@RequestParam(value = "region_id", required = false) Integer regionId) {
		List<City> citiesList = userService.getCitiesList(regionId);
		
		String json = "{\"cities\": [";
		for (City each : citiesList) {
			json = json + "{\"id\":" + each.getId() + ",\"name\":\"" + each.getName() + "\"},";
		}
		json = json.substring(0, json.length() - 1);
		json = json + "]}";
		
		return json;
	}
}