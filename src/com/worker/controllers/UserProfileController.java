package com.worker.controllers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.worker.entities.City;
import com.worker.entities.Contact;
import com.worker.entities.Country;
import com.worker.entities.Region;
import com.worker.entities.User;
import com.worker.services.UserService;

@Controller
public class UserProfileController {

	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/profile.action", method = RequestMethod.GET)
	public ModelAndView getUserProfile(
			@CookieValue(value = "uuid", required = false) String uuid,
			@RequestParam(value = "saveChange", required = false) String saveChange) {
		ModelAndView mav = new ModelAndView("userprofile");

		if (uuid != null) {
			User user = userService.getUser(uuid);

			List<Country> countriesList = userService.getCountriesList();
			mav.addObject("countriesList", countriesList);

			List<Region> regionsList = userService.getRegionsList(user
					.getCountry().getId());
			mav.addObject("regionsList", regionsList);

			List<City> citiesList = userService.getCitiesList(user.getRegion()
					.getId());
			mav.addObject("citiesList", citiesList);

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String birthDate = dateFormat.format(user.getBirthDate());
			String registrationDate = dateFormat.format(user.getRegistrationDate());
			// Передает переменную на jsp страницу
			mav.addObject("user", user);
			mav.addObject("birthDate", birthDate);
			mav.addObject("registrationDate", registrationDate);
		}

		if (saveChange != null) {
			mav.addObject("saveChange", true);
		}
		return mav;
	}

	@RequestMapping(value = "/profile.action", method = RequestMethod.POST)
	public ModelAndView postUserProfile(
			@CookieValue(value = "uuid", required = false) String uuid,
			@RequestParam(value = "first_name", required = false) String firstName,
			@RequestParam(value = "surname", required = false) String surname,
			@RequestParam(value = "last_name", required = false) String lastName,
			@RequestParam(value = "birth_date", required = false) String birthDateText,
			@RequestParam(value = "phones", required = false) String phones,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "skype", required = false) String skype,
			@RequestParam(value = "facebook", required = false) String facebook,
			@RequestParam(value = "vkontakte", required = false) String vkontakte,
			@RequestParam(value = "twitter", required = false) String twitter,
			@RequestParam(value = "icq", required = false) String icq,
			@RequestParam(value = "google_plus", required = false) String google_plus,
			@RequestParam(value = "country_id", required = false) Integer countryId,
			@RequestParam(value = "region_id", required = false) Integer regionId,
			@RequestParam(value = "city_id", required = false) Integer cityId,
			@RequestParam(value = "gender", required = false) String genderText,
			@RequestParam(value = "profession", required = false) String profession,
			@RequestParam(value = "position", required = false) String position,
			@RequestParam(value = "experience", required = false) String experience) {
		ModelAndView mav = new ModelAndView("userprofile");

		if (uuid != null) {
			User user = userService.getUser(uuid);
			mav.addObject("user", user);

			user.setFirstName(firstName);

			user.setSurname(surname);

			user.setLastName(lastName);

			mav.addObject("birthDateText", birthDateText);
			Date birthDate = null;

			if (!birthDateText.isEmpty()) {
				try {
					birthDate = new SimpleDateFormat("yyyy-MM-dd")
							.parse(birthDateText);
					birthDate = new java.sql.Date(birthDate.getTime());
				} catch (ParseException e) {
					e.printStackTrace();
				}
				user.setBirthDate(birthDate);
			}

			Contact contact = user.getContact();

			contact.setPhones(phones);

			contact.setEmail(email);

			contact.setSkype(skype);

			contact.setFacebook(facebook);

			contact.setVkontakte(vkontakte);

			contact.setTwitter(twitter);

			contact.setIcq(icq);

			contact.setGooglePlus(google_plus);

			Country country = new Country();
			country.setId(countryId);
			user.setCountry(country);

			if (regionId != null) {
				Region region = new Region();
				region.setId(regionId);
				user.setRegion(region);
			}

			user.setRegistrationDate(new Date());

			int gender = Integer.parseInt(genderText);
			user.setGender(gender);

			user.setProfession(profession);

			user.setPosition(position);

			user.setExperience(experience);

			if (cityId == null && regionId == null) {
				List<Country> countriesList = userService.getCountriesList();
				mav.addObject("countriesList", countriesList);

				List<Region> regionsList = userService
						.getRegionsList(countryId);
				mav.addObject("regionsList", regionsList);
			}

			if (cityId == null) {
				List<Country> countriesList = userService.getCountriesList();
				mav.addObject("countriesList", countriesList);

				List<City> citiesList = userService.getCitiesList(regionId);
				mav.addObject("citiesList", citiesList);

				List<Region> regionsList = userService
						.getRegionsList(countryId);
				mav.addObject("regionsList", regionsList);
			}

			City city = new City();
			city.setId(cityId);
			user.setCity(city);

			userService.updateUser(user);
			userService.updateContact(contact);
			return new ModelAndView("redirect:profile.action?saveChange");
		}
		return mav;
	}
	
	@RequestMapping(value = "/edit.action", method = RequestMethod.GET)
	public ModelAndView getEditProfile(
			@CookieValue(value = "uuid", required = false) String uuid,
			@RequestParam(value = "saveChange", required = false) String saveChange) {
		ModelAndView mav = new ModelAndView("edit");

		if (uuid != null) {
			User user = userService.getUser(uuid);

			List<Country> countriesList = userService.getCountriesList();
			mav.addObject("countriesList", countriesList);

			List<Region> regionsList = userService.getRegionsList(user
					.getCountry().getId());
			mav.addObject("regionsList", regionsList);

			List<City> citiesList = userService.getCitiesList(user.getRegion()
					.getId());
			mav.addObject("citiesList", citiesList);

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String birthDate = dateFormat.format(user.getBirthDate());
			String registrationDate = dateFormat.format(user.getRegistrationDate());
			// Передает переменную на jsp страницу
			mav.addObject("user", user);
			mav.addObject("birthDate", birthDate);
			mav.addObject("registrationDate", registrationDate);
		}

		if (saveChange != null) {
			mav.addObject("saveChange", true);
		}
		return mav;
	}

	@RequestMapping(value = "/edit.action", method = RequestMethod.POST)
	public ModelAndView postEditProfile(
			@CookieValue(value = "uuid", required = false) String uuid,
			@RequestParam(value = "first_name", required = false) String firstName,
			@RequestParam(value = "surname", required = false) String surname,
			@RequestParam(value = "last_name", required = false) String lastName,
			@RequestParam(value = "birth_date", required = false) String birthDateText,
			@RequestParam(value = "phones", required = false) String phones,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "skype", required = false) String skype,
			@RequestParam(value = "facebook", required = false) String facebook,
			@RequestParam(value = "vkontakte", required = false) String vkontakte,
			@RequestParam(value = "twitter", required = false) String twitter,
			@RequestParam(value = "icq", required = false) String icq,
			@RequestParam(value = "google_plus", required = false) String google_plus,
			@RequestParam(value = "country_id", required = false) Integer countryId,
			@RequestParam(value = "region_id", required = false) Integer regionId,
			@RequestParam(value = "city_id", required = false) Integer cityId,
			@RequestParam(value = "gender", required = false) String genderText,
			@RequestParam(value = "profession", required = false) String profession,
			@RequestParam(value = "position", required = false) String position,
			@RequestParam(value = "experience", required = false) String experience) {
		ModelAndView mav = new ModelAndView("edit");

		if (uuid != null) {
			User user = userService.getUser(uuid);
			mav.addObject("user", user);

			user.setFirstName(firstName);

			user.setSurname(surname);

			user.setLastName(lastName);

			mav.addObject("birthDateText", birthDateText);
			Date birthDate = null;

			if (!birthDateText.isEmpty()) {
				try {
					birthDate = new SimpleDateFormat("yyyy-MM-dd")
							.parse(birthDateText);
					birthDate = new java.sql.Date(birthDate.getTime());
				} catch (ParseException e) {
					e.printStackTrace();
				}
				user.setBirthDate(birthDate);
			}

			Contact contact = user.getContact();

			contact.setPhones(phones);

			contact.setEmail(email);

			contact.setSkype(skype);

			contact.setFacebook(facebook);

			contact.setVkontakte(vkontakte);

			contact.setTwitter(twitter);

			contact.setIcq(icq);

			contact.setGooglePlus(google_plus);

			Country country = new Country();
			country.setId(countryId);
			user.setCountry(country);

			if (regionId != null) {
				Region region = new Region();
				region.setId(regionId);
				user.setRegion(region);
			}

			user.setRegistrationDate(new Date());

			int gender = Integer.parseInt(genderText);
			user.setGender(gender);

			user.setProfession(profession);

			user.setPosition(position);

			user.setExperience(experience);

			if (cityId == null && regionId == null) {
				List<Country> countriesList = userService.getCountriesList();
				mav.addObject("countriesList", countriesList);

				List<Region> regionsList = userService
						.getRegionsList(countryId);
				mav.addObject("regionsList", regionsList);
			}

			if (cityId == null) {
				List<Country> countriesList = userService.getCountriesList();
				mav.addObject("countriesList", countriesList);

				List<City> citiesList = userService.getCitiesList(regionId);
				mav.addObject("citiesList", citiesList);

				List<Region> regionsList = userService
						.getRegionsList(countryId);
				mav.addObject("regionsList", regionsList);
			}

			City city = new City();
			city.setId(cityId);
			user.setCity(city);

			userService.updateUser(user);
			userService.updateContact(contact);
			return new ModelAndView("redirect:edit.action?saveChange");
		}
		return mav;
	}

}
