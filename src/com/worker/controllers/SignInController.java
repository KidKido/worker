package com.worker.controllers;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.worker.services.UserService;

@Controller
public class SignInController {

	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/signin.action", method = RequestMethod.GET)
	public ModelAndView getSignIn(
			@RequestParam(value = "notregistered", required = false) String notregistered) {
		ModelAndView mav = new ModelAndView("signin");

		if (notregistered == null) {
			mav.addObject("notRegistered", false);
		} else {
			mav.addObject("notRegistered", true);
		}

		return mav;
	}

	@RequestMapping(value = "/signin.action", method = RequestMethod.POST)
	public ModelAndView postSignIn(
			@RequestParam(value = "signout", required = false) String signout,
			HttpServletRequest request, HttpServletResponse response) {

		if (signout != null) {
			Cookie cookie = userService.signOut(request.getCookies());
			response.addCookie(cookie);
			return new ModelAndView("redirect:index.action");
		}

		String login = request.getParameter("login");
		String password = request.getParameter("password");

		Cookie cookie = userService.getAuthorizationCookie(login, password);
		if (cookie != null) {
			response.addCookie(cookie);
			return new ModelAndView("redirect:index.action");
		}

		return new ModelAndView("redirect:signin.action?notregistered");
	}

}
