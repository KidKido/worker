package com.worker.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.worker.entities.Tender;
import com.worker.entities.User;
import com.worker.services.TenderService;
import com.worker.services.UserService;

@Controller
public class MyTenderController {
	
	@Autowired
	private TenderService tenderService;
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/mytender.action", method = RequestMethod.GET)
	public ModelAndView getMyTender(@CookieValue(value = "uuid", required = true) String uuid) {
		ModelAndView mav = new ModelAndView("mytender");
		
		if (uuid != null) {
			User user = userService.getUser(uuid);
			mav.addObject("user", user);
			List<Tender> userTendersList = tenderService.getUserTendersList(user.getId());
			mav.addObject("userTendersList", userTendersList);
		}
		
		return mav;
	}

}
