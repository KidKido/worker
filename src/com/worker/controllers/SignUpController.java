package com.worker.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.worker.entities.City;
import com.worker.entities.Contact;
import com.worker.entities.Country;
import com.worker.entities.Region;
import com.worker.entities.User;
import com.worker.services.UserService;

@Controller
public class SignUpController {

	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/signup.action", method = RequestMethod.GET)
	public ModelAndView getSignUp() {
		ModelAndView mav = new ModelAndView("signup");

		List<Country> countriesList = userService.getCountriesList();
		mav.addObject("countriesList", countriesList);
		return mav;
	}
	
	@RequestMapping(value = "/signup.action", method = RequestMethod.POST)
	public ModelAndView postSignUp(
			@RequestParam(value = "login", required = true) String login,
			@RequestParam(value = "password", required = true) String password,
			@RequestParam(value = "first_name", required = true) String firstName,
			@RequestParam(value = "surname", required = true) String surname,
			@RequestParam(value = "last_name", required = false) String lastName,
			@RequestParam(value = "birth_date", required = true) String birthDateText,
			@RequestParam(value = "phones", required = true) String phones,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "country_id", required = true) Integer countryId,
			@RequestParam(value = "region_id", required = false) Integer regionId,
			@RequestParam(value = "city_id", required = true) Integer cityId,
			@RequestParam(value = "gender", required = true) Integer gender,
			@RequestParam(value = "profession", required = true, defaultValue = "") String profession,
			@RequestParam(value = "position", required = true, defaultValue = "") String position,
			@RequestParam(value = "work_form", required = true) Integer role,
			@RequestParam(value = "experience", required = true) String experience, 
			HttpServletResponse response) {

		User user = new User();
		Contact contacts = new Contact();

		user.setContact(contacts);
		user.setLogin(login);
		user.setPassword(password);
		user.setFirstName(firstName);
		user.setSurname(surname);
		user.setLastName(lastName);

		Date birthDate = null;

		if (!birthDateText.isEmpty()) {
			try {
				birthDate = new SimpleDateFormat("yyyy-MM-dd")
						.parse(birthDateText);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			user.setBirthDate(birthDate);
		}

		contacts.setPhones(phones);
		contacts.setEmail(email);

		Country country = new Country();
		country.setId(countryId);
		user.setCountry(country);

		if (regionId != null) {
			Region region = new Region();
			region.setId(regionId);
			user.setRegion(region);
		}

		user.setRegistrationDate(new Date());
		user.setGender(gender);
		user.setProfession(profession);
		user.setPosition(position);
		user.setRole(role);
		user.setExperience(experience);

		City city = new City();
		city.setId(cityId);
		user.setCity(city);

		String uuid = UUID.randomUUID().toString();
		user.setUuid(uuid);
		userService.saveUser(user);
		
		Cookie cookie = userService.getAuthorizationCookie(login, password);
		response.addCookie(cookie);

		return new ModelAndView("redirect:index.action");
	}
	
	@RequestMapping(value = "/check-login.action", method = RequestMethod.GET)
	@ResponseBody
	public String getCheckLogin(@RequestParam(value = "login", required = true) String login) {
		boolean isAvailableLogin = userService.isAvailableLogin(login);
		String json = "{\"isAvailableLogin\": " + isAvailableLogin + "}";
		
		return json;
	}

	@RequestMapping(value = "/check-email.action", method = RequestMethod.GET)
	@ResponseBody
	public String getCheckEmail(@RequestParam(value = "email", required = true) String email) {
		boolean isAvailableEmail = userService.isAvailableEmail(email);
		String json = "{\"isAvailableEmail\": " + isAvailableEmail + "}";
		
		return json;
	}
	
}