package com.worker.services;

import java.util.List;
import java.util.UUID;

import javax.servlet.http.Cookie;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.worker.dao.UserDao;
import com.worker.entities.City;
import com.worker.entities.Contact;
import com.worker.entities.Country;
import com.worker.entities.Region;
import com.worker.entities.User;

@Service
public class UserService {
	
	@Autowired
	public UserDao userDao;

	@Transactional
	public List<User> getUserList() {
		return userDao.getUserList();
	}

	@Transactional
	public List<Country> getCountriesList() {
		return userDao.getCountriesList();
	}

	@Transactional
	public List<Region> getRegionsList(int countryId) {
		return userDao.getRegionsList(countryId);
	}

	@Transactional
	public List<City> getCitiesList(int regionId) {
		return userDao.getCitiesList(regionId);
	}

	@Transactional
	public void saveUser(User user) {
		userDao.saveUser(user);
	}

	@Transactional
	public Cookie getAuthorizationCookie(String login, String password) {
		boolean isExists = userDao.isExists(login, password);

		if (isExists) {
			String uuid = UUID.randomUUID().toString();
			userDao.saveUUID(login, uuid);
			Cookie cookie = new Cookie("uuid", uuid);
			return cookie;
		}

		return null;
	}

	@Transactional
	public boolean isVerificationUser(String login, String password) {
		return userDao.isExists(login, password);
	}

	@Transactional
	public User getUser(String uuid) {
		User user = userDao.getUser(uuid);
		return user;
	}
	
	@Transactional
	public Cookie signOut(Cookie[] cookies) {
		for (Cookie each : cookies) {
			if (each.getName().equals("uuid")) {
				String uuid = each.getValue();
				userDao.signOut(uuid);
				each.setMaxAge(0);
				return each;
			}
		}

		return null;
	}

	@Transactional
	public void updateUser(User user) {
		userDao.updateUser(user);
	}
	
	@Transactional
	public void updateContact(Contact contact) {
		userDao.updateContact(contact);
	}
	
	@Transactional
	public Country getCountry(Integer countryId) {
		return userDao.getCountry(countryId);
	}
	
	@Transactional
	public Region getRegion(Integer cityId) {
		return userDao.getRegion(cityId);
	}
	
	@Transactional
	public City getCity(Integer cityId) {
		return userDao.getCity(cityId);
	}
	
	@Transactional
	public boolean isAvailableLogin(String login) {
		return userDao.isAvailableLogin(login);
	}

	@Transactional
	public boolean isAvailableEmail(String email) {
		return userDao.isAvailableEmail(email);
	}

}