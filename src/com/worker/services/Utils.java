package com.worker.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Utils {
	
	public static void executeSql(String sql) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		try (Connection connection = DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/worker", "root", "1234");
				Statement statement = connection.createStatement();) {
			statement.execute(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
