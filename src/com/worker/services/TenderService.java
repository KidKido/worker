
package com.worker.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.worker.dao.TenderDao;
import com.worker.entities.Tender;
import com.worker.entities.TenderMessage;

@Service
public class TenderService {
	
	@Autowired
	private TenderDao tenderDao;

	@Transactional
	public List<Tender> getTendersList(String search, Integer countryId, Integer regionId, Integer cityId, int page) {
		List<Tender> tendersList = tenderDao.getTendersList(search, countryId, regionId, cityId, page);
		return tendersList;
	}
	
	@Transactional
	public Tender getTender(Integer tenderId) {
		Tender tender = tenderDao.getTender(tenderId);
		return tender;
	}

	@Transactional
	public List<TenderMessage> getMessagesList(Integer tenderId) {
		List<TenderMessage> tenderMessagesList = tenderDao
				.getMessagesList(tenderId);
		return tenderMessagesList;
	}

	@Transactional
	public void addTenderMessage(TenderMessage tenderMessage, Integer tenderId) {
		tenderDao.addTenderMessage(tenderMessage, tenderId);
	}
	
	@Transactional
	public void addNewTender(Tender tender) {
		tenderDao.addNewTender(tender);
	}
	
	@Transactional
	public List<Tender> getUserTendersList(int userId) {
		List<Tender> userTendersList = tenderDao.getUserTendersList(userId);
		return userTendersList;
	}
	
	@Transactional
	public int getPagesCount(String search, Integer countryId, Integer regionId, Integer cityId) {
		int pagesCount = tenderDao.getPagesCount(search, countryId, regionId, cityId);
		pagesCount = (int) (Math.ceil((pagesCount * 1.0) / 10));
		
		return pagesCount;
	}
}